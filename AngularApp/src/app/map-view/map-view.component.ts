import { Item } from '../model/item';
import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.css']
})
export class MapViewComponent implements OnInit {
  items: Item;

  constructor(private http: Http) { }

  ngOnInit() {
    this.http.get('/api/food/getall')
      .subscribe(res => {
        this.items = res.json();
      });
  }

}
