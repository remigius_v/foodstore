import {Item} from '../model/item';
import {Component, OnInit} from '@angular/core';
import {NgForm, FormGroup, FormBuilder} from '@angular/forms';
import {Http} from '@angular/http';
import {MarkerManager, AgmMarker , LatLngLiteral} from '@agm/core';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  item: FormGroup;
  marker: AgmMarker;
  lon = 77.5967788696289;
  lat = 12.963743284940753;
  constructor(private http: Http, private fb: FormBuilder, private mm: MarkerManager) {}

  ngOnInit() {
    this.item = this.fb.group({
      name: '', price: '', qty: '',
      address: this.fb.group({lat: '', lon: ''})
    });
  }

  setLocation(loc: any) {
    const locLiterals: LatLngLiteral = {lat: loc.coords.lat, lng: loc.coords.lng};
    console.log(loc.coords);
    this.item.controls['address'].setValue({lat: loc.coords.lat, lon: loc.coords.lng});
    this.marker = new AgmMarker(this.mm);
    this.marker.latitude = loc.coords.lat;
    this.marker.longitude =  loc.coords.lng;
    this.mm.addMarker(this.marker);
    console.log(this.lat);
  }

  public onSave(form: NgForm) {
    let item: Item;


    item = form.value;
    console.log(item);
    this.http.post('/supplier/food', item)
      .subscribe(res => {
        console.log('added');
      });
  }
}
