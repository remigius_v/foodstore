export class Item {
  id: number;
  name: string;
  price: number;
  qty: number;
  address: Address;
}

export class Address {
  lat: string;
  lon: string;
}
