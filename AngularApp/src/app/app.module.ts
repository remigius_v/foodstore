import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {AgmCoreModule, MarkerManager, GoogleMapsAPIWrapper} from '@agm/core';

import {AppComponent} from './app.component';
import {appRoutes} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NavigationComponent} from './comp/navigation/navigation.component';
import {MapViewComponent} from './map-view/map-view.component';
import {Error404Component} from './error404/error404.component';
import {ListViewComponent} from './list-view/list-view.component';
import {RouterModule} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {AddItemComponent} from './add-item/add-item.component';
import { LoadingComponent } from './comp/loading/loading.component';
import { FooterComponent } from './comp/footer/footer.component';
import { DetailsComponent } from './page/details/details.component';
import { RegisterComponent } from './page/register/register.component';
import { LoginComponent } from './page/login/login.component';
import { LoginStatusService } from './service/login-status.service';


@NgModule({
declarations: [
AppComponent,
NavigationComponent,
MapViewComponent,
Error404Component,
ListViewComponent,
IndexComponent,
AddItemComponent,
LoadingComponent,
FooterComponent,
DetailsComponent,
RegisterComponent,
LoginComponent
],
imports: [
BrowserModule,
HttpModule,
FormsModule,
ReactiveFormsModule,
AgmCoreModule.forRoot({apiKey: 'AIzaSyCbjwqbvP7g_vLr8hkaTBtJ-AHsni61NOw'}),
    RouterModule.forRoot(
      appRoutes
    )],
  providers: [ MarkerManager, GoogleMapsAPIWrapper, LoginStatusService ],
  bootstrap: [AppComponent]
})
export class AppModule {}

