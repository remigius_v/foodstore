import { AddItemComponent } from './add-item/add-item.component';
import {Error404Component} from './error404/error404.component';
import { IndexComponent } from './index/index.component';
import {ListViewComponent} from './list-view/list-view.component';
import {MapViewComponent} from './map-view/map-view.component';
import {LoginComponent} from './page/login/login.component';
import {RegisterComponent} from './page/register/register.component';
import {Routes} from '@angular/router';
export const appRoutes: Routes = [
{path: '', component: IndexComponent},
{path: 'login', component: LoginComponent},
{path: 'register', component: RegisterComponent},
{path: 'map-view', component: MapViewComponent},
{path: 'list-view', component: ListViewComponent},
{path: 'add-item', component: AddItemComponent},
{path: '**', component: Error404Component}
];
export class AppRoutingModule {}
