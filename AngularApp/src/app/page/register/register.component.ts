import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Http} from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userForm: FormGroup;
  constructor(private http: Http, private fb: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.userForm = this.fb.group({
      name: '',
      password: '',
      confirm_password: '',
      email: ''
    });
  }
  register(form: FormGroup) {
    console.log(form.value);
    this.http.post('user/register', form.value)
      .subscribe(res => {
        console.log(res);
        this.router.navigateByUrl('/');
      });
  }


}
