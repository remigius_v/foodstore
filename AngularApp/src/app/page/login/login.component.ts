import {LoginStatusService} from '../../service/login-status.service';
import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Http, RequestOptions} from '@angular/http';
import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  constructor(private http: Http, private fb: FormBuilder, private router: Router, private loginStatus: LoginStatusService) {}

  ngOnInit() {
    this.userForm = this.fb.group({
      name: '',
      password: ''
    });
  }
  register(form: FormGroup) {
    const formData = new FormData();
    formData.append('name', form.value.name);
    formData.append('password', form.value.password);
    this.http.post('user/login', formData)
      .subscribe(res => {
        this.loginStatus.post(form.value.name);
        this.router.navigateByUrl('/');
      });
  }

}
