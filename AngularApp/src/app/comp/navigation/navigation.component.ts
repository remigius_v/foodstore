import {LoginStatusService} from '../../service/login-status.service';
import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  username: any;
  subscription: Subscription;
  constructor(private loginStatus: LoginStatusService) {}

  ngOnInit() {
    this.subscription = this.loginStatus.subscribe().subscribe(
      user => {
        this.username = user;
      }
    );
  }
  logout(){
    this.loginStatus.logout();
  }

}
