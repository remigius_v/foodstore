import {Item} from '../model/item';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {Http} from '@angular/http';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {
  title = 'Food Store';
  items: Item[];
  constructor(private http: Http) {

  }
  ngOnInit() {

    this.http.get('/api/food/getall')
      .subscribe(res => {
        this.items = res.json();
      });
  }
  public delete(item) {
    this.http.post('/api/food/delete', item)
      .subscribe(res => {
        console.log('deleted');
        this.items = this.items.filter(i => i !== item);
      });
  }


}

