import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs';

@Injectable()
export class LoginStatusService {

  private user ;

  constructor(private http: Http) {}

  post(name: string) {
    this.user.next(name);
  }
  subscribe(): Observable<any> {
    return this.user.asObservable();
  }
  logout() {
    this.http.get('/user/logout').subscribe(
      res => {
        this.post('guest');
      }
    );

  }
}
