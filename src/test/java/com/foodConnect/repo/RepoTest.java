package com.foodConnect.repo;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.foodConnect.model.Item;

@DataJpaTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class RepoTest {
	@Autowired
	ItemRepo itemRepo;

	@Test
	public void ItemRepoTest(){

		List<Item> items =new ArrayList<>();
		items.add(new Item("Apple",10,50.0));
		items.add(new Item("Orange",20,30.0));
		items.add(new Item("Bannana",30,10.0));
		items.add(new Item("Pinnapple",15,80.0));

		items.add(new Item("Mango",15,80.0));
		
		itemRepo.save(items);
	}

}
