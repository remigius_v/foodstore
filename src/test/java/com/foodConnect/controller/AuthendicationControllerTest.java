package com.foodConnect.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.foodConnect.model.user.Role;
import com.foodConnect.model.user.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthendicationControllerTest {
	
	@MockBean
	MockMvc mockMvc;
	
	@Autowired
	BCryptPasswordEncoder encoder;
	
	public void testRegisterUser() throws Exception {
		User u=new User();
		u.setName("Test User");
		u.setPassword(encoder.encode("testpassword"));
		Set<Role> roles = new TreeSet<>();
		roles.add(new Role("ADMIN"));
		u.setRoles(roles);
		
		mockMvc.perform(post("/api/user",u))
		.andExpect(status().isOk());
		//User newUser = this.testRestTemplate.postForObject("/api/user", u, User.class);
		//System.out.println(newUser);
	}

}
