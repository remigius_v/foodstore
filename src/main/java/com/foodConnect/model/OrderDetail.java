package com.foodConnect.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class OrderDetail {

	@Id
	@GeneratedValue
	Long id;
	@OneToMany(cascade = CascadeType.DETACH,fetch = FetchType.EAGER)
	List<Item> items;
}
