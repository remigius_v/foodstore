package com.foodConnect.model.user;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class User {

	@Id
	String name;
	String email;
	String password;
	@ManyToMany(fetch=FetchType.EAGER,cascade = CascadeType.MERGE)
	@JoinTable(name="user_role",joinColumns=@JoinColumn(name="user_name"),
			inverseJoinColumns = @JoinColumn(name="role_id"))
	Set<Role> Roles;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Set<Role> getRoles() {
		return Roles;
	}
	public void setRoles(Set<Role> roles) {
		Roles = roles;
	}
	@Override
	public String toString() {
		return "User [name=" + name + ", email=" + email + ", password=" + password + ", Roles=" + Roles + "]";
	}
}
