package com.foodConnect.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity(name ="Orders") //Order is a keyword
public class Order {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	Long id;
	@OneToOne(fetch= FetchType.EAGER)
	Address destination;
	@OneToOne(fetch= FetchType.EAGER)
	OrderDetail details;
	Timestamp createdDate;
	Timestamp deliveryDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Address getDestination() {
		return destination;
	}
	public void setDestination(Address destination) {
		this.destination = destination;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Timestamp deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
}
