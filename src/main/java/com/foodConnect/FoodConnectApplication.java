package com.foodConnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodConnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodConnectApplication.class, args);
	}
}
