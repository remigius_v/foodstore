package com.foodConnect.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Remigius on 16-12-2017.
 */
@Configuration
public class MvcConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    PublicPageUrl publicPageUrl;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        for (String publicUrl : publicPageUrl.getUrls().toArray(new String[0]))
            registry.addViewController(publicUrl).setViewName("index");
    }
}
