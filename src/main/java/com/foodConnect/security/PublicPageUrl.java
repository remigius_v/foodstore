package com.foodConnect.security;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Remigius on 08-11-2017.
 */
@Component
public class PublicPageUrl {
    static private List<String> urls;
    static {
        urls=new ArrayList<>();
        urls.add("/");
        urls.add("/login");
        urls.add("/register");
        urls.add("/api/**");
        urls.add("/list-view");
        urls.add("/map-view");
        urls.add("/item/**");
    }

    public List<String> getUrls() {
        return urls;
    }
}
