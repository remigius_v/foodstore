package com.foodConnect.security;

import java.util.Collection;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.foodConnect.model.user.User;
@Component
public class UserDetailsImpl implements UserDetails {
	
	private static final long serialVersionUID = 1L;
	private User user;

	final static Logger log = Logger.getLogger(UserDetailsImpl.class);
	public UserDetailsImpl(){
		
	}
	public UserDetailsImpl(User user){
		this.user = user;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	

}
