package com.foodConnect.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodConnect.model.Item;

public interface ItemRepo extends JpaRepository<Item,Long>  {

}
