package com.foodConnect.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodConnect.model.Address;

@Repository
public interface AddressRepo extends JpaRepository<Address,Long>  {

}
