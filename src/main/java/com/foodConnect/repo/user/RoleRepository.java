package com.foodConnect.repo.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodConnect.model.user.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,String> {

}
