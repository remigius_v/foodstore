package com.foodConnect.repo.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodConnect.model.user.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>  {

}
