package com.foodConnect.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foodConnect.exception.NotFound;
import com.foodConnect.model.Item;
import com.foodConnect.service.DataService;

@RestController
public class FoodController {

	@Autowired
	DataService dataService;

	@GetMapping("/api/food/getall")
	public List<Item> getAllFood() {
		return dataService.getAllItems();
	}

	@DeleteMapping("/supplier/food")
	public ResponseEntity deleteFood(@RequestParam long id,Principal p) {
		dataService.deleteItem(id,p);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@PatchMapping("/supplier/food")
	public ResponseEntity<Item> deleteFood1(@RequestBody Item item,Principal p) {
		try {
			item = dataService.update(item,p);
		} catch (NotFound e) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Item>(item,HttpStatus.OK);
	}

	@PostMapping("/supplier/food")
	public ResponseEntity<Item> addFood(@RequestBody Item item,Principal p) {
		return new ResponseEntity<Item>(dataService.addItem(item,p), HttpStatus.OK);
	}

}
