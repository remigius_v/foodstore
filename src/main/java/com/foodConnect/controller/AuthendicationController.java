package com.foodConnect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foodConnect.model.user.User;
import com.foodConnect.service.UserService;

@RestController
@RequestMapping("user")
public class AuthendicationController {
	
	@Autowired
	UserService userService;
	
	@PostMapping("register")
	public ResponseEntity<User> registerUser(@RequestBody User newUser){
		System.out.println(newUser);
		User u = userService.createNewUser(newUser);
		return new ResponseEntity<User>(u,HttpStatus.CREATED);
	}
}
