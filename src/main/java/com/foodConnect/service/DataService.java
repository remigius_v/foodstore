package com.foodConnect.service;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodConnect.exception.NotFound;
import com.foodConnect.model.Item;
import com.foodConnect.repo.ItemRepo;

@Service
public class DataService {
	
	@Autowired
	ItemRepo itemRepo;

	public List<Item> getAllItems() {
		return itemRepo.findAll();
	}

	public void deleteItem(Long id, Principal p) {
		itemRepo.delete(id);
		
	}

	public  Item addItem(Item item, Principal p) {
		Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
		item.setSupplierName(p.getName());
		item.setCreatedOn(currentTimestamp);
		return itemRepo.save(item);
	}

	public Item update(Item item, Principal p) throws NotFound {
		if(item.getId()!=null && !itemRepo.exists(item.getId()))
			throw new NotFound("Item :"+item.getId());
		item.setSupplierName(p.getName());
		return itemRepo.save(item);		
		
	}

}
