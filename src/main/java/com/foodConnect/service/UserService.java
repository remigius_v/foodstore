package com.foodConnect.service;


import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.foodConnect.model.user.Role;
import com.foodConnect.model.user.User;
import com.foodConnect.repo.user.UserRepository;

@Service
public class UserService {
	
	static final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	UserRepository userRepo;
	@Autowired
	BCryptPasswordEncoder encoder;

	public User createNewUser(User newUser) {
		newUser.setPassword(encoder.encode(newUser.getPassword()));
		
		Set<Role> roles = new HashSet<Role>();
		roles.add(new Role("CUSTOMER"));
		newUser.setRoles(roles);
		log.info("New user registering..."+ newUser);
		return userRepo.save(newUser);		
	}
	
	
}
