webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/add-item/add-item.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/add-item/add-item.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"fh5co-pricing-section\"\r\n\tclass=\"fh5co-pricing fh5co-lightgray-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-8 col-md-offset-2\">\r\n\t\t\t\t<div class=\"heading-section text-center \">\r\n\t\t\t\t\t<h2>Add new item</h2>\r\n\t\t\t\t\t<p></p>\r\n\t\t\t\t\t<form [formGroup]=\"item\" novalidate (ngSubmit)=\"onSave(item)\">\r\n\t\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t\t<div class=\"row col-xs-12 col-sm-6 col-md-6 col-lg-6\">\r\n\t\t\t\t\t\t\t\t\t<span> <input class=\"clean-slide form-control\" id=\"name\"\r\n\t\t\t\t\t\t\t\t\t\ttype=\"text\" formControlName=\"name\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Item name here\" required /> <label for=\"name\">Name</label>\r\n\t\t\t\t\t\t\t\t\t</span> <span> <input class=\"clean-slide form-control\"\r\n\t\t\t\t\t\t\t\t\t\tid=\"price\" type=\"text\" formControlName=\"price\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Item price here\" required /> <label for=\"price\">Price</label>\r\n\t\t\t\t\t\t\t\t\t</span> <span> <input class=\"clean-slide form-control\" id=\"qty\"\r\n\t\t\t\t\t\t\t\t\t\ttype=\"text\" formControlName=\"qty\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Item quantity here\" required /> <label\r\n\t\t\t\t\t\t\t\t\t\tfor=\"price\">Qty</label>\r\n\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t<fieldset formGroupName=\"address\">\r\n\t\t\t\t\t\t\t\t\t\t<span> <input class=\"clean-slide form-control\"\r\n\t\t\t\t\t\t\t\t\t\t\tid=\"long\" type=\"text\" formControlName=\"lon\" ngModel\r\n\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Longitude here\" required /> <label for=\"lon\">Lon</label>\r\n\t\t\t\t\t\t\t\t\t\t</span> <span> <input class=\"clean-slide form-control\"\r\n\t\t\t\t\t\t\t\t\t\t\tid=\"lat\" type=\"text\" formControlName=\"lat\" ngModel\r\n\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Lattitude here\" required /> <label for=\"lat\">Lat</label>\r\n\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"row col-xs-12 col-sm-6 col-md-6 col-lg-6\">\r\n\t\t\t\t\t\t\t\t\t<agm-map class=\"add-map\" [latitude]=\"lat\" [longitude]=\"lon\"\r\n\t\t\t\t\t\t\t\t\t\t(mapClick)=\"setLocation($event)\"> <agm-marker\r\n\t\t\t\t\t\t\t\t\t\t[latitude]=\"lat\" [longitude]=\"lon\"></agm-marker> </agm-map>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"row text-center\">\r\n\t\t\t\t\t\t\t\t<input class=\"disabled\" type=\"submit\" class=\"btn fill-btn\"\r\n\t\t\t\t\t\t\t\t\tvalue=\"Save\" />\r\n\r\n\t\t\t\t\t\t\t\t<button (click)=\"showAdd=!showAdd\" class=\"btn\">Cancel</button>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/add-item/add-item.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddItemComponent = (function () {
    function AddItemComponent(http, fb, mm) {
        this.http = http;
        this.fb = fb;
        this.mm = mm;
        this.lon = 77.5967788696289;
        this.lat = 12.963743284940753;
    }
    AddItemComponent.prototype.ngOnInit = function () {
        this.item = this.fb.group({
            name: '', price: '', qty: '',
            address: this.fb.group({ lat: '', lon: '' })
        });
    };
    AddItemComponent.prototype.setLocation = function (loc) {
        var locLiterals = { lat: loc.coords.lat, lng: loc.coords.lng };
        console.log(loc.coords);
        this.item.controls['address'].setValue({ lat: loc.coords.lat, lon: loc.coords.lng });
        this.marker = new __WEBPACK_IMPORTED_MODULE_3__agm_core__["b" /* AgmMarker */](this.mm);
        this.marker.latitude = loc.coords.lat;
        this.marker.longitude = loc.coords.lng;
        this.mm.addMarker(this.marker);
        console.log(this.lat);
    };
    AddItemComponent.prototype.onSave = function (form) {
        var item;
        item = form.value;
        console.log(item);
        this.http.post('/supplier/food', item)
            .subscribe(function (res) {
            console.log('added');
        });
    };
    AddItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-item',
            template: __webpack_require__("../../../../../src/app/add-item/add-item.component.html"),
            styles: [__webpack_require__("../../../../../src/app/add-item/add-item.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__agm_core__["d" /* MarkerManager */]])
    ], AddItemComponent);
    return AddItemComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n\r\n\r\n<div id=\"fh5co-wrapper\">\r\n\t<div id=\"fh5co-page\">\r\n\t\t<app-navigation></app-navigation>\r\n\t\t<router-outlet></router-outlet>\r\n\t\t<!-- fh5co-blog-section -->\r\n\t\t<app-footer></app-footer>\r\n\r\n\t</div>\r\n\t<!-- END fh5co-page -->\r\n\r\n</div>\r\n<!-- END fh5co-wrapper -->\r\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Food Store';
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_routes__ = __webpack_require__("../../../../../src/app/app.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__comp_navigation_navigation_component__ = __webpack_require__("../../../../../src/app/comp/navigation/navigation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__map_view_map_view_component__ = __webpack_require__("../../../../../src/app/map-view/map-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__error404_error404_component__ = __webpack_require__("../../../../../src/app/error404/error404.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__list_view_list_view_component__ = __webpack_require__("../../../../../src/app/list-view/list-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__index_index_component__ = __webpack_require__("../../../../../src/app/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__add_item_add_item_component__ = __webpack_require__("../../../../../src/app/add-item/add-item.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__comp_loading_loading_component__ = __webpack_require__("../../../../../src/app/comp/loading/loading.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__comp_footer_footer_component__ = __webpack_require__("../../../../../src/app/comp/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__page_details_details_component__ = __webpack_require__("../../../../../src/app/page/details/details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__page_register_register_component__ = __webpack_require__("../../../../../src/app/page/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__page_login_login_component__ = __webpack_require__("../../../../../src/app/page/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__service_login_status_service__ = __webpack_require__("../../../../../src/app/service/login-status.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_time_ago_pipe__ = __webpack_require__("../../../../time-ago-pipe/time-ago-pipe.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_time_ago_pipe___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_20_time_ago_pipe__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__comp_navigation_navigation_component__["a" /* NavigationComponent */],
                __WEBPACK_IMPORTED_MODULE_8__map_view_map_view_component__["a" /* MapViewComponent */],
                __WEBPACK_IMPORTED_MODULE_9__error404_error404_component__["a" /* Error404Component */],
                __WEBPACK_IMPORTED_MODULE_10__list_view_list_view_component__["a" /* ListViewComponent */],
                __WEBPACK_IMPORTED_MODULE_12__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_13__add_item_add_item_component__["a" /* AddItemComponent */],
                __WEBPACK_IMPORTED_MODULE_14__comp_loading_loading_component__["a" /* LoadingComponent */],
                __WEBPACK_IMPORTED_MODULE_15__comp_footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_16__page_details_details_component__["a" /* DetailsComponent */],
                __WEBPACK_IMPORTED_MODULE_17__page_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_18__page_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_20_time_ago_pipe__["TimeAgoPipe"]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["c" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__agm_core__["a" /* AgmCoreModule */].forRoot({ apiKey: 'AIzaSyCbjwqbvP7g_vLr8hkaTBtJ-AHsni61NOw' }),
                __WEBPACK_IMPORTED_MODULE_11__angular_router__["b" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_routes__["a" /* appRoutes */])
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_3__agm_core__["d" /* MarkerManager */], __WEBPACK_IMPORTED_MODULE_3__agm_core__["c" /* GoogleMapsAPIWrapper */], __WEBPACK_IMPORTED_MODULE_19__service_login_status_service__["a" /* LoginStatusService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return appRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__add_item_add_item_component__ = __webpack_require__("../../../../../src/app/add-item/add-item.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__error404_error404_component__ = __webpack_require__("../../../../../src/app/error404/error404.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("../../../../../src/app/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_view_list_view_component__ = __webpack_require__("../../../../../src/app/list-view/list-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__map_view_map_view_component__ = __webpack_require__("../../../../../src/app/map-view/map-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__page_login_login_component__ = __webpack_require__("../../../../../src/app/page/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__page_register_register_component__ = __webpack_require__("../../../../../src/app/page/register/register.component.ts");







var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_5__page_login_login_component__["a" /* LoginComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_6__page_register_register_component__["a" /* RegisterComponent */] },
    { path: 'map-view', component: __WEBPACK_IMPORTED_MODULE_4__map_view_map_view_component__["a" /* MapViewComponent */] },
    { path: 'list-view', component: __WEBPACK_IMPORTED_MODULE_3__list_view_list_view_component__["a" /* ListViewComponent */] },
    { path: 'add-item', component: __WEBPACK_IMPORTED_MODULE_0__add_item_add_item_component__["a" /* AddItemComponent */] },
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_1__error404_error404_component__["a" /* Error404Component */] }
];


/***/ }),

/***/ "../../../../../src/app/comp/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/comp/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "\t<footer>\r\n\t\t\t<div id=\"footer\">\r\n\t\t\t\t<div class=\"container\">\r\n\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t\t\t<h3 class=\"section-title\">About Us</h3>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>\r\n\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t<div class=\"col-md-4 animate-box\">\r\n\t\t\t\t\t\t\t<h3 class=\"section-title\">Our Address</h3>\r\n\t\t\t\t\t\t\t<ul class=\"contact-info\">\r\n\t\t\t\t\t\t\t\t<li><i class=\"icon-map-marker\"></i>198 West 21th Street, Suite 721 New York NY 10016</li>\r\n\t\t\t\t\t\t\t\t<li><i class=\"icon-phone\"></i>+ 1235 2355 98</li>\r\n\t\t\t\t\t\t\t\t<li><i class=\"icon-envelope\"></i><a routerLink=\"/\">info@yoursite.com</a></li>\r\n\t\t\t\t\t\t\t\t<li><i class=\"icon-globe2\"></i><a routerLink=\"/\">www.yoursite.com</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-4 animate-box\">\r\n\t\t\t\t\t\t\t<h3 class=\"section-title\">Drop us a line</h3>\r\n\t\t\t\t\t\t\t<form class=\"contact-form\">\r\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t<label for=\"name\" class=\"sr-only\">Name</label>\r\n\t\t\t\t\t\t\t\t\t<input type=\"name\" class=\"form-control\" id=\"name\" placeholder=\"Name\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t<label for=\"email\" class=\"sr-only\">Email</label>\r\n\t\t\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Email\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t<label for=\"message\" class=\"sr-only\">Message</label>\r\n\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"message\" rows=\"7\" placeholder=\"Message\"></textarea>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t<input type=\"submit\" id=\"btn-submit\" class=\"btn btn-send-message btn-md\" value=\"Send Message\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</form>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row copy-right\">\r\n\t\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center\">\r\n\t\t\t\t\t\t\t<p class=\"fh5co-social-icons\">\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-twitter2\"></i></a>\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-facebook2\"></i></a>\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-instagram\"></i></a>\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-dribbble2\"></i></a>\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-youtube\"></i></a>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t<p>Copyright 2016 <a routerLink=\"/\">FoodStore</a>. All Rights Reserved. </p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</footer>"

/***/ }),

/***/ "../../../../../src/app/comp/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/comp/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/comp/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/comp/loading/loading.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/comp/loading/loading.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  loading works!\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/app/comp/loading/loading.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoadingComponent = (function () {
    function LoadingComponent() {
    }
    LoadingComponent.prototype.ngOnInit = function () {
    };
    LoadingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-loading',
            template: __webpack_require__("../../../../../src/app/comp/loading/loading.component.html"),
            styles: [__webpack_require__("../../../../../src/app/comp/loading/loading.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LoadingComponent);
    return LoadingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/comp/navigation/navigation.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/comp/navigation/navigation.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"fh5co-header\">\r\n\t<header id=\"fh5co-header-section\">\r\n\t\t<div class=\"container\">\r\n\t\t\t<div class=\"nav-header\">\r\n\t\t\t\t<a routerLink=\"/\" class=\"js-fh5co-nav-toggle fh5co-nav-toggle\"><i></i></a>\r\n\t\t\t\t<h1 id=\"fh5co-logo\">\r\n\t\t\t\t\t<a>Food<span>store</span></a>\r\n\t\t\t\t</h1>\r\n\t\t\t\t<!-- START #fh5co-menu-wrap -->\r\n\t\t\t\t<nav id=\"fh5co-menu-wrap\" role=\"navigation\">\r\n\t\t\t\t\t<ul class=\"sf-menu\" id=\"fh5co-primary-menu\">\r\n\t\t\t\t\t\t<li routerLinkActive=\"active\"><a routerLink=\"/\">Home</a></li>\r\n\t\t\t\t\t\t<li routerLinkActive=\"active\" *ngIf=\"username=='guest'\"><a routerLink=\"/login\">Login</a></li>\r\n\t\t\t\t\t\t<li (click)=\"logout()\" *ngIf=\"username!='guest'\"><a>Logout</a></li>\r\n\t\t\t\t\t\t<li routerLinkActive=\"active\" *ngIf=\"username=='guest'\"><a routerLink=\"/register\">Register</a></li>\r\n\t\t\t\t\t\t<li routerLinkActive=\"active\"><a routerLink=\"/add-item\">Add</a></li>\r\n\r\n\t\t\t\t\t\t<li><a href=\"classes.html\" class=\"fh5co-sub-ddown\">Search</a>\r\n\t\t\t\t\t\t\t<ul class=\"fh5co-sub-menu\">\r\n\t\t\t\t\t\t\t\t<li routerLinkActive=\"active\"><a routerLink=\"/list-view\">List\r\n\t\t\t\t\t\t\t\tview</a></li>\r\n\t\t\t\t\t\t<li routerLinkActive=\"active\"><a routerLink=\"/map-view\">Map\r\n\t\t\t\t\t\t\t\tView</a></li>\r\n\r\n\t\t\t\t\t\t\t</ul></li>\r\n\t\t\t\t\t</ul>\r\n\t\t\t\t</nav>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</header>\r\n</div>\r\n<!-- end:fh5co-header -->\r\n<div class=\"fh5co-hero\" style=\"height: 100px\">\r\n\t<div class=\"fh5co-overlay\" style=\"height: 100px\"></div>\r\n\t<div class=\"fh5co-cover\" data-stellar-background-ratio=\"0.5\"\r\n\t\tstyle=\"height: 100px; background-image: url(assets/images/home-image.jpg);\">\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/comp/navigation/navigation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__service_login_status_service__ = __webpack_require__("../../../../../src/app/service/login-status.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavigationComponent = (function () {
    function NavigationComponent(loginStatus) {
        this.loginStatus = loginStatus;
    }
    NavigationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.loginStatus.subscribe().subscribe(function (user) {
            _this.username = user;
        });
    };
    NavigationComponent.prototype.logout = function () {
        this.loginStatus.logout();
    };
    NavigationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-navigation',
            template: __webpack_require__("../../../../../src/app/comp/navigation/navigation.component.html"),
            styles: [__webpack_require__("../../../../../src/app/comp/navigation/navigation.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__service_login_status_service__["a" /* LoginStatusService */]])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/error404/error404.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/error404/error404.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"fh5co-hero\">\r\n\t<div class=\"fh5co-overlay\"></div>\r\n\t<div class=\"fh5co-cover\" data-stellar-background-ratio=\"0.5\"\r\n\t\tstyle=\"background-image: url(assets/images/home-image.jpg);\">\r\n\t\t<div class=\"desc\">\r\n\t\t\t<div class=\"container\">\r\n\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t<div class=\"col-md-7\">\r\n\t\t\t\t\t\t<h2>\r\n\t\t\t\t\t\t\tOops! page is not availale! \r\n\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t<span><a class=\"btn btn-primary\" routerLink=\"/\">Start\r\n\t\t\t\t\t\t\t\tHome</a></span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/error404/error404.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Error404Component; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Error404Component = (function () {
    function Error404Component() {
    }
    Error404Component.prototype.ngOnInit = function () {
    };
    Error404Component = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-error404',
            template: __webpack_require__("../../../../../src/app/error404/error404.component.html"),
            styles: [__webpack_require__("../../../../../src/app/error404/error404.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], Error404Component);
    return Error404Component;
}());



/***/ }),

/***/ "../../../../../src/app/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"fh5co-hero\">\r\n\t<div class=\"fh5co-overlay\"></div>\r\n\t<div class=\"fh5co-cover\" data-stellar-background-ratio=\"0.5\"\r\n\t\tstyle=\"background-image: url(assets/images/home-image.jpg);\">\r\n\t\t<div class=\"desc\">\r\n\t\t\t<div class=\"container\">\r\n\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t<div class=\"col-md-7\">\r\n\t\t\t\t\t\t<h2>\r\n\t\t\t\t\t\t\tFood &amp; Health <br>is a <b>Priority</b>\r\n\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t<p>\r\n\r\n\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t<span><a class=\"btn btn-primary\" routerLink=\"list-view\">Find\r\n\t\t\t\t\t\t\t\tYour Food</a></span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<!-- end:fh5co-hero -->\r\n<!--<div id=\"fh5co-schedule-section\" class=\"fh5co-lightgray-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-8 col-md-offset-2\">\r\n\t\t\t\t<div class=\"heading-section text-center \">\r\n\t\t\t\t\t<h2>Class Schedule</h2>\r\n\t\t\t\t\t<p>Separated they live in Bookmarksgrove right at the coast of\r\n\t\t\t\t\t\tthe Semantics, a large language ocean.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row \">\r\n\t\t\t<div class=\"col-md-10 col-md-offset-1 text-center\">\r\n\t\t\t\t<ul class=\"schedule\">\r\n\t\t\t\t\t<li><a routerLink=\"/\" class=\"active\" data-sched=\"sunday\">Sunday</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"/\" data-sched=\"monday\">Monday</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"/\" data-sched=\"tuesday\">Tuesday</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"/\" data-sched=\"wednesday\">Wednesday</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"/\" data-sched=\"thursday\">Thursday</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"/\" data-sched=\"monday\">Monday</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"/\" data-sched=\"saturday\">Saturday</a></li>\r\n\t\t\t\t</ul>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"row text-center\">\r\n\r\n\t\t\t\t<div class=\"col-md-12 schedule-container\">\r\n\r\n\t\t\t\t\t<div class=\"schedule-content active\" data-day=\"sunday\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-dumbell.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Body Building</h3>\r\n\t\t\t\t\t\t\t\t<span>John Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-yoga.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Yoga Programs</h3>\r\n\t\t\t\t\t\t\t\t<span>James Smith</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-cycling.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Cycling Program</h3>\r\n\t\t\t\t\t\t\t\t<span>Rita Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-boxing.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Boxing Fitness</h3>\r\n\t\t\t\t\t\t\t\t<span>John Dose</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t&lt;!&ndash; END sched-content &ndash;&gt;\r\n\r\n\t\t\t\t\t<div class=\"schedule-content\" data-day=\"monday\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-yoga.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Yoga Programs</h3>\r\n\t\t\t\t\t\t\t\t<span>James Smith</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-dumbell.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Body Building</h3>\r\n\t\t\t\t\t\t\t\t<span>John Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-boxing.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Boxing Fitness</h3>\r\n\t\t\t\t\t\t\t\t<span>John Dose</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-cycling.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Cycling Program</h3>\r\n\t\t\t\t\t\t\t\t<span>Rita Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t&lt;!&ndash; END sched-content &ndash;&gt;\r\n\r\n\t\t\t\t\t<div class=\"schedule-content\" data-day=\"tuesday\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-dumbell.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Body Building</h3>\r\n\t\t\t\t\t\t\t\t<span>John Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-yoga.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Yoga Programs</h3>\r\n\t\t\t\t\t\t\t\t<span>James Smith</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-cycling.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Cycling Program</h3>\r\n\t\t\t\t\t\t\t\t<span>Rita Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-boxing.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Boxing Fitness</h3>\r\n\t\t\t\t\t\t\t\t<span>John Dose</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t&lt;!&ndash; END sched-content &ndash;&gt;\r\n\r\n\t\t\t\t\t<div class=\"schedule-content\" data-day=\"wednesday\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-yoga.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Yoga Programs</h3>\r\n\t\t\t\t\t\t\t\t<span>James Smith</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-dumbell.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Body Building</h3>\r\n\t\t\t\t\t\t\t\t<span>John Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-boxing.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Boxing Fitness</h3>\r\n\t\t\t\t\t\t\t\t<span>John Dose</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-cycling.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Cycling Program</h3>\r\n\t\t\t\t\t\t\t\t<span>Rita Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t&lt;!&ndash; END sched-content &ndash;&gt;\r\n\r\n\t\t\t\t\t<div class=\"schedule-content\" data-day=\"thursday\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-dumbell.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Body Building</h3>\r\n\t\t\t\t\t\t\t\t<span>John Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-yoga.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Yoga Programs</h3>\r\n\t\t\t\t\t\t\t\t<span>James Smith</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-cycling.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Cycling Program</h3>\r\n\t\t\t\t\t\t\t\t<span>Rita Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-boxing.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Boxing Fitness</h3>\r\n\t\t\t\t\t\t\t\t<span>John Dose</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t&lt;!&ndash; END sched-content &ndash;&gt;\r\n\r\n\t\t\t\t\t<div class=\"schedule-content\" data-day=\"friday\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-yoga.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Yoga Programs</h3>\r\n\t\t\t\t\t\t\t\t<span>James Smith</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-dumbell.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Body Building</h3>\r\n\t\t\t\t\t\t\t\t<span>John Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-boxing.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Boxing Fitness</h3>\r\n\t\t\t\t\t\t\t\t<span>John Dose</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-cycling.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Cycling Program</h3>\r\n\t\t\t\t\t\t\t\t<span>Rita Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t&lt;!&ndash; END sched-content &ndash;&gt;\r\n\r\n\t\t\t\t\t<div class=\"schedule-content\" data-day=\"saturday\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-dumbell.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Body Building</h3>\r\n\t\t\t\t\t\t\t\t<span>John Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-yoga.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Yoga Programs</h3>\r\n\t\t\t\t\t\t\t\t<span>James Smith</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-cycling.svg\" alt=\"\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Cycling Program</h3>\r\n\t\t\t\t\t\t\t\t<span>Rita Doe</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t\t\t\t\t\t<div class=\"program program-schedule\">\r\n\t\t\t\t\t\t\t\t<img src=\"assets/images/fit-boxing.svg\" alt=\"Cycling\"> <small>06AM-7AM</small>\r\n\t\t\t\t\t\t\t\t<h3>Boxing Fitness</h3>\r\n\t\t\t\t\t\t\t\t<span>John Dose</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t&lt;!&ndash; END sched-content &ndash;&gt;\r\n\t\t\t\t</div>\r\n\r\n\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<div class=\"fh5co-parallax\"\r\n\tstyle=\"background-image: url(assets/images/home-image-3.jpg);\"\r\n\tdata-stellar-background-ratio=\"0.5\">\r\n\t<div class=\"overlay\"></div>\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div\r\n\t\t\t\tclass=\"col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table\">\r\n\t\t\t\t<div class=\"fh5co-intro fh5co-table-cell \">\r\n\t\t\t\t\t<h1 class=\"text-center\">Commit To Be Fit</h1>\r\n\t\t\t\t\t<p>\r\n\t\t\t\t\t\tMade with love by the fine folks at <a href=\"http://freehtml5.co\">FreeHTML5.co</a>\r\n\t\t\t\t\t</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n&lt;!&ndash; end: fh5co-parallax &ndash;&gt;\r\n<div id=\"fh5co-programs-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-8 col-md-offset-2\">\r\n\t\t\t\t<div class=\"heading-section text-center \">\r\n\t\t\t\t\t<h2>Our Programs</h2>\r\n\t\t\t\t\t<p>Separated they live in Bookmarksgrove right at the coast of\r\n\t\t\t\t\t\tthe Semantics, a large language ocean.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row text-center\">\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"program \">\r\n\t\t\t\t\t<img src=\"assets/images/fit-dumbell.svg\" alt=\"Cycling\">\r\n\t\t\t\t\t<h3>Body Combat</h3>\r\n\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t<span><a routerLink=\"/\" class=\"btn btn-default\">Join Now</a></span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"program \">\r\n\t\t\t\t\t<img src=\"assets/images/fit-yoga.svg\" alt=\"\">\r\n\t\t\t\t\t<h3>Yoga Programs</h3>\r\n\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t<span><a routerLink=\"/\" class=\"btn btn-default\">Join Now</a></span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"program \">\r\n\t\t\t\t\t<img src=\"assets/images/fit-cycling.svg\" alt=\"\">\r\n\t\t\t\t\t<h3>Cycling Program</h3>\r\n\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t<span><a routerLink=\"/\" class=\"btn btn-default\">Join Now</a></span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"program \">\r\n\t\t\t\t\t<img src=\"assets/images/fit-boxing.svg\" alt=\"Cycling\">\r\n\t\t\t\t\t<h3>Boxing Fitness</h3>\r\n\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t<span><a routerLink=\"/\" class=\"btn btn-default\">Join Now</a></span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"program \">\r\n\t\t\t\t\t<img src=\"assets/images/fit-swimming.svg\" alt=\"\">\r\n\t\t\t\t\t<h3>Swimming Program</h3>\r\n\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t<span><a routerLink=\"/\" class=\"btn btn-default\">Join Now</a></span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"program \">\r\n\t\t\t\t\t<img src=\"assets/images/fit-massage.svg\" alt=\"\">\r\n\t\t\t\t\t<h3>Massage</h3>\r\n\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t<span><a routerLink=\"/\" class=\"btn btn-default\">Join Now</a></span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<div id=\"fh5co-team-section\" class=\"fh5co-lightgray-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-8 col-md-offset-2\">\r\n\t\t\t\t<div class=\"heading-section text-center \">\r\n\t\t\t\t\t<h2>Meet Our Trainers</h2>\r\n\t\t\t\t\t<p>Separated they live in Bookmarksgrove right at the coast of\r\n\t\t\t\t\t\tthe Semantics, a large language ocean.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row text-center\">\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"team-section-grid \"\r\n\t\t\t\t\tstyle=\"background-image: url(assets/images/trainer-1.jpg);\">\r\n\t\t\t\t\t<div class=\"overlay-section\">\r\n\t\t\t\t\t\t<div class=\"desc\">\r\n\t\t\t\t\t\t\t<h3>John Doe</h3>\r\n\t\t\t\t\t\t\t<span>Body Trainer</span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\t\tcountries Vokalia and Consonantia</p>\r\n\t\t\t\t\t\t\t<p class=\"fh5co-social-icons\">\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-twitter-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-facebook-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-instagram-with-circle\"></i></a>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"team-section-grid \"\r\n\t\t\t\t\tstyle=\"background-image: url(assets/images/trainer-2.jpg);\">\r\n\t\t\t\t\t<div class=\"overlay-section\">\r\n\t\t\t\t\t\t<div class=\"desc\">\r\n\t\t\t\t\t\t\t<h3>James Smith</h3>\r\n\t\t\t\t\t\t\t<span>Swimming Trainer</span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\t\tcountries Vokalia and Consonantia</p>\r\n\t\t\t\t\t\t\t<p class=\"fh5co-social-icons\">\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-twitter-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-facebook-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-instagram-with-circle\"></i></a>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"team-section-grid \"\r\n\t\t\t\t\tstyle=\"background-image: url(assets/images/trainer-3.jpg);\">\r\n\t\t\t\t\t<div class=\"overlay-section\">\r\n\t\t\t\t\t\t<div class=\"desc\">\r\n\t\t\t\t\t\t\t<h3>John Doe</h3>\r\n\t\t\t\t\t\t\t<span>Chief Executive Officer</span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t\t\t<p class=\"fh5co-social-icons\">\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-twitter-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-facebook-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-instagram-with-circle\"></i></a>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"team-section-grid \"\r\n\t\t\t\t\tstyle=\"background-image: url(assets/images/trainer-4.jpg);\">\r\n\t\t\t\t\t<div class=\"overlay-section\">\r\n\t\t\t\t\t\t<div class=\"desc\">\r\n\t\t\t\t\t\t\t<h3>John Doe</h3>\r\n\t\t\t\t\t\t\t<span>Chief Executive Officer</span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t\t\t<p class=\"fh5co-social-icons\">\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-twitter-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-facebook-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-instagram-with-circle\"></i></a>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"team-section-grid \"\r\n\t\t\t\t\tstyle=\"background-image: url(assets/images/trainer-5.jpg);\">\r\n\t\t\t\t\t<div class=\"overlay-section\">\r\n\t\t\t\t\t\t<div class=\"desc\">\r\n\t\t\t\t\t\t\t<h3>John Doe</h3>\r\n\t\t\t\t\t\t\t<span>Chief Executive Officer</span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t\t\t<p class=\"fh5co-social-icons\">\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-twitter-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-facebook-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-instagram-with-circle\"></i></a>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-4 col-sm-6\">\r\n\t\t\t\t<div class=\"team-section-grid \"\r\n\t\t\t\t\tstyle=\"background-image: url(assets/images/trainer-6.jpg);\">\r\n\t\t\t\t\t<div class=\"overlay-section\">\r\n\t\t\t\t\t\t<div class=\"desc\">\r\n\t\t\t\t\t\t\t<h3>John Doe</h3>\r\n\t\t\t\t\t\t\t<span>Chief Executive Officer</span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\t\tcountries Vokalia and Consonantia, there live the blind texts.</p>\r\n\t\t\t\t\t\t\t<p class=\"fh5co-social-icons\">\r\n\t\t\t\t\t\t\t\t<a routerLink=\"/\"><i class=\"icon-twitter-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-facebook-with-circle\"></i></a> <a\r\n\t\t\t\t\t\t\t\t\trouterLink=\"/\"><i class=\"icon-instagram-with-circle\"></i></a>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<div class=\"fh5co-parallax\"\r\n\tstyle=\"background-image: url(assets/images/home-image-2.jpg);\"\r\n\tdata-stellar-background-ratio=\"0.5\">\r\n\t<div class=\"overlay\"></div>\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div\r\n\t\t\t\tclass=\"col-md-6 col-md-offset-3 col-md-pull-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 fh5co-table\">\r\n\t\t\t\t<div class=\"fh5co-intro fh5co-table-cell box-area\">\r\n\t\t\t\t\t<div class=\"\">\r\n\t\t\t\t\t\t<h1>Fitness Classes this summer</h1>\r\n\t\t\t\t\t\t<p>Pay now and get 25% Discount</p>\r\n\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-primary\">Become A Member</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>-->\r\n<!-- end: fh5co-parallax -->\r\n<!-- <div id=\"fh5co-pricing-section\"\r\n\tclass=\"fh5co-pricing fh5co-lightgray-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-8 col-md-offset-2\">\r\n\t\t\t\t<div class=\"heading-section text-center \">\r\n\t\t\t\t\t<h2>Pricing Plan</h2>\r\n\t\t\t\t\t<p>Separated they live in Bookmarksgrove right at the coast of\r\n\t\t\t\t\t\tthe Semantics, a large language ocean.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"pricing\">\r\n\t\t\t\t<div class=\"col-md-3 \">\r\n\t\t\t\t\t<div class=\"price-box \">\r\n\t\t\t\t\t\t<h2 class=\"pricing-plan\">Starter</h2>\r\n\t\t\t\t\t\t<div class=\"price\">\r\n\t\t\t\t\t\t\t<sup class=\"currency\">$</sup>9<small>/month</small>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\tcountries Vokalia and Consonantia</p>\r\n\t\t\t\t\t\t<ul class=\"classes\">\r\n\t\t\t\t\t\t\t<li>15 Cardio Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">10 Swimming Lesson</li>\r\n\t\t\t\t\t\t\t<li>10 Yoga Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">20 Aerobics</li>\r\n\t\t\t\t\t\t\t<li>10 Zumba Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">5 Massage</li>\r\n\t\t\t\t\t\t\t<li>10 Body Building</li>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-default\">Select Plan</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<div class=\"col-md-3 \">\r\n\t\t\t\t\t<div class=\"price-box \">\r\n\t\t\t\t\t\t<h2 class=\"pricing-plan\">Basic</h2>\r\n\t\t\t\t\t\t<div class=\"price\">\r\n\t\t\t\t\t\t\t<sup class=\"currency\">$</sup>27<small>/month</small>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\tcountries Vokalia and Consonantia</p>\r\n\t\t\t\t\t\t<ul class=\"classes\">\r\n\t\t\t\t\t\t\t<li>15 Cardio Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">10 Swimming Lesson</li>\r\n\t\t\t\t\t\t\t<li>10 Yoga Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">20 Aerobics</li>\r\n\t\t\t\t\t\t\t<li>10 Zumba Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">5 Massage</li>\r\n\t\t\t\t\t\t\t<li>10 Body Building</li>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-default\">Select Plan</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<div class=\"col-md-3 \">\r\n\t\t\t\t\t<div class=\"price-box  popular\">\r\n\t\t\t\t\t\t<h2 class=\"pricing-plan pricing-plan-offer\">\r\n\t\t\t\t\t\t\tPro <span>Best Offer</span>\r\n\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t<div class=\"price\">\r\n\t\t\t\t\t\t\t<sup class=\"currency\">$</sup>74<small>/month</small>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\tcountries Vokalia and Consonantia</p>\r\n\t\t\t\t\t\t<ul class=\"classes\">\r\n\t\t\t\t\t\t\t<li>15 Cardio Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">10 Swimming Lesson</li>\r\n\t\t\t\t\t\t\t<li>10 Yoga Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">20 Aerobics</li>\r\n\t\t\t\t\t\t\t<li>10 Zumba Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">5 Massage</li>\r\n\t\t\t\t\t\t\t<li>10 Body Building</li>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-select-plan btn-sm\">Select\r\n\t\t\t\t\t\t\tPlan</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<div class=\"col-md-3 \">\r\n\t\t\t\t\t<div class=\"price-box \">\r\n\t\t\t\t\t\t<h2 class=\"pricing-plan\">Unlimited</h2>\r\n\t\t\t\t\t\t<div class=\"price\">\r\n\t\t\t\t\t\t\t<sup class=\"currency\">$</sup>140<small>/month</small>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\tcountries Vokalia and Consonantia</p>\r\n\t\t\t\t\t\t<ul class=\"classes\">\r\n\t\t\t\t\t\t\t<li>15 Cardio Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">10 Swimming Lesson</li>\r\n\t\t\t\t\t\t\t<li>10 Yoga Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">20 Aerobics</li>\r\n\t\t\t\t\t\t\t<li>10 Zumba Classes</li>\r\n\t\t\t\t\t\t\t<li class=\"color\">5 Massage</li>\r\n\t\t\t\t\t\t\t<li>10 Body Building</li>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-default\">Select Plan</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div> -->\r\n<!--\r\n<div id=\"fh5co-blog-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<div class=\"heading-section \">\r\n\t\t\t\t\t\t<h2>Recent from Blog</h2>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-12 col-md-offset-0\">\r\n\t\t\t\t\t<div class=\"fh5co-blog \">\r\n\t\t\t\t\t\t<div class=\"inner-post\">\r\n\t\t\t\t\t\t\t<a routerLink=\"/\"><img class=\"img-responsive\"\r\n\t\t\t\t\t\t\t\tsrc=\"assets/images/blog-1.jpg\" alt=\"\"></a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"desc\">\r\n\t\t\t\t\t\t\t<h3>\r\n\t\t\t\t\t\t\t\t<a href=\"\">Starting new session of body building this summer</a>\r\n\t\t\t\t\t\t\t</h3>\r\n\t\t\t\t\t\t\t<span class=\"posted_by\">Posted by: Admin</span> <span\r\n\t\t\t\t\t\t\t\tclass=\"comment\"><a href=\"\">21<i class=\"icon-bubble22\"></i></a></span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains</p>\r\n\t\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-default\">Read More</a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-12 col-md-offset-0\">\r\n\t\t\t\t\t<div class=\"fh5co-blog \">\r\n\t\t\t\t\t\t<div class=\"inner-post\">\r\n\t\t\t\t\t\t\t<a routerLink=\"/\"><img class=\"img-responsive\"\r\n\t\t\t\t\t\t\t\tsrc=\"assets/images/blog-1.jpg\" alt=\"\"></a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"desc\">\r\n\t\t\t\t\t\t\t<h3>\r\n\t\t\t\t\t\t\t\t<a href=\"\">Starting new session of body building this summer</a>\r\n\t\t\t\t\t\t\t</h3>\r\n\t\t\t\t\t\t\t<span class=\"posted_by\">Posted by: Admin</span> <span\r\n\t\t\t\t\t\t\t\tclass=\"comment\"><a href=\"\">21<i class=\"icon-bubble22\"></i></a></span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains</p>\r\n\t\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-default\">Read More</a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<div class=\"heading-section \">\r\n\t\t\t\t\t\t<h2>Upcoming Events</h2>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-12 col-md-offset-0\">\r\n\t\t\t\t\t<div class=\"fh5co-blog \">\r\n\t\t\t\t\t\t<div class=\"meta-date text-center\">\r\n\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t<span class=\"date\">14</span><span>June</span><span>2016</span>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"desc desc2\">\r\n\t\t\t\t\t\t\t<h3>\r\n\t\t\t\t\t\t\t\t<a href=\"\">Starting new session of body building this summer</a>\r\n\t\t\t\t\t\t\t</h3>\r\n\t\t\t\t\t\t\t<span class=\"posted_by\">Posted by: Admin</span> <span\r\n\t\t\t\t\t\t\t\tclass=\"comment\"><a href=\"\">21<i class=\"icon-bubble22\"></i></a></span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\t\tcountries Vokalia and Consonantia</p>\r\n\t\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-default\">Read More</a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-12 col-md-offset-0\">\r\n\t\t\t\t\t<div class=\"fh5co-blog \">\r\n\t\t\t\t\t\t<div class=\"meta-date text-center\">\r\n\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t<span class=\"date\">13</span><span>June</span><span>2016</span>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"desc desc2\">\r\n\t\t\t\t\t\t\t<h3>\r\n\t\t\t\t\t\t\t\t<a href=\"\">Starting new session of body building this summer</a>\r\n\t\t\t\t\t\t\t</h3>\r\n\t\t\t\t\t\t\t<span class=\"posted_by\">Posted by: Admin</span> <span\r\n\t\t\t\t\t\t\t\tclass=\"comment\"><a href=\"\">21<i class=\"icon-bubble22\"></i></a></span>\r\n\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the\r\n\t\t\t\t\t\t\t\tcountries Vokalia and Consonantia</p>\r\n\t\t\t\t\t\t\t<a routerLink=\"/\" class=\"btn btn-default\">Read More</a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div> -->\r\n"

/***/ }),

/***/ "../../../../../src/app/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IndexComponent = (function () {
    function IndexComponent() {
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], IndexComponent);
    return IndexComponent;
}());



/***/ }),

/***/ "../../../../../src/app/list-view/list-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/list-view/list-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"fh5co-pricing-section\"\r\n\tclass=\"fh5co-pricing fh5co-lightgray-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-8 col-md-offset-2\">\r\n\t\t\t\t<div class=\"heading-section text-center \">\r\n\t\t\t\t\t<h2>Available items</h2>\r\n\t\t\t\t\t<p></p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"pricing\">\r\n\t\t\t\t<div *ngFor=\"let item of items\">\r\n\t\t\t\t\t<div class=\"price-box \">\r\n\t\t\t\t\t\t<h2 class=\"pricing-plan\">{{item.name}}</h2>\r\n\t\t\t\t\t\t<div class=\"price\">\r\n\t\t\t\t\t\t\t<sup class=\"currency\"></sup>{{item.price| currency:\"&#8377;\"}}<small>/kg</small>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<p></p>\r\n\t\t\t\t\t\t<ul class=\"classes\">\r\n\t\t\t\t\t\t\t<li class=\"color\">{{item.qty}} Qty</li>\r\n\t\t\t\t\t\t\t<li >{{item.createdOn| timeAgo }}</li>\r\n\t\t\t\t\t\t\t<li >posted by <b>{{item.supplierName}}</b></li>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t<a [routerLink]=\"['../item',item.id ]\" class=\"btn btn-default\">Details</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/list-view/list-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListViewComponent = (function () {
    function ListViewComponent(http) {
        this.http = http;
        this.title = 'Food Store';
    }
    ListViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get('/api/food/getall')
            .subscribe(function (res) {
            _this.items = res.json();
        });
    };
    ListViewComponent.prototype.delete = function (item) {
        var _this = this;
        this.http.post('/api/food/delete', item)
            .subscribe(function (res) {
            console.log('deleted');
            _this.items = _this.items.filter(function (i) { return i !== item; });
        });
    };
    ListViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-list-view',
            template: __webpack_require__("../../../../../src/app/list-view/list-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/list-view/list-view.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], ListViewComponent);
    return ListViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/map-view/map-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/map-view/map-view.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  Coming soon!\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/app/map-view/map-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MapViewComponent = (function () {
    function MapViewComponent(http) {
        this.http = http;
    }
    MapViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get('/api/food/getall')
            .subscribe(function (res) {
            _this.items = res.json();
        });
    };
    MapViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-map-view',
            template: __webpack_require__("../../../../../src/app/map-view/map-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/map-view/map-view.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], MapViewComponent);
    return MapViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/page/details/details.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/page/details/details.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  details works!\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/app/page/details/details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DetailsComponent = (function () {
    function DetailsComponent() {
    }
    DetailsComponent.prototype.ngOnInit = function () {
    };
    DetailsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-details',
            template: __webpack_require__("../../../../../src/app/page/details/details.component.html"),
            styles: [__webpack_require__("../../../../../src/app/page/details/details.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DetailsComponent);
    return DetailsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/page/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/page/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"fh5co-pricing-section\"\r\n\tclass=\"fh5co-pricing fh5co-lightgray-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-8 col-md-offset-2\">\r\n\t\t\t\t<div class=\"heading-section text-center \">\r\n\t\t\t\t\t<h2>Login</h2>\r\n\t\t\t\t\t<p></p>\r\n\t\t\t\t\t<form [formGroup]=\"userForm\" novalidate (ngSubmit)=\"register(userForm)\">\r\n\t\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t\t<div class=\"row col-xs-12 col-sm-6 col-md-6 col-lg-6\">\r\n\t\t\t\t\t\t\t\t\t<span> <input class=\"clean-slide form-control\" id=\"name\"\r\n\t\t\t\t\t\t\t\t\t\ttype=\"text\" formControlName=\"name\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Name here\" required /> <label for=\"name\">Username</label>\r\n\t\t\t\t\t\t\t\t\t</span> <span> <input class=\"clean-slide form-control\"\r\n\t\t\t\t\t\t\t\t\t\tid=\"password\" type=\"text\" formControlName=\"password\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"password here\" required /> <label for=\"password\">Password</label>\r\n\t\t\t\t\t\t\t\t\t</span> \r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"row text-center\">\r\n\t\t\t\t\t\t\t\t<input type=\"submit\" class=\"btn fill-btn\"\r\n\t\t\t\t\t\t\t\t\tvalue=\"Login\" />\r\n\t\t\t\t\t\t\t\t<button routerLink=\"/register\" class=\"btn\">Register?</button>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/page/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__service_login_status_service__ = __webpack_require__("../../../../../src/app/service/login-status.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = (function () {
    function LoginComponent(http, fb, router, loginStatus) {
        this.http = http;
        this.fb = fb;
        this.router = router;
        this.loginStatus = loginStatus;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.userForm = this.fb.group({
            name: '',
            password: ''
        });
    };
    LoginComponent.prototype.register = function (form) {
        var _this = this;
        var formData = new FormData();
        formData.append('name', form.value.name);
        formData.append('password', form.value.password);
        this.http.post('user/login', formData)
            .subscribe(function (res) {
            _this.loginStatus.post(form.value.name);
            _this.router.navigateByUrl('/');
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/page/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/page/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */], __WEBPACK_IMPORTED_MODULE_0__service_login_status_service__["a" /* LoginStatusService */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/page/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/page/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"fh5co-pricing-section\"\r\n\tclass=\"fh5co-pricing fh5co-lightgray-section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-md-8 col-md-offset-2\">\r\n\t\t\t\t<div class=\"heading-section text-center \">\r\n\t\t\t\t\t<h2>Register new user</h2>\r\n\t\t\t\t\t<p></p>\r\n\t\t\t\t\t<form [formGroup]=\"userForm\" novalidate (ngSubmit)=\"register(userForm)\">\r\n\t\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t\t<div class=\"row col-xs-12 col-sm-6 col-md-6 col-lg-6\">\r\n\t\t\t\t\t\t\t\t\t<span> <input class=\"clean-slide form-control\" id=\"name\"\r\n\t\t\t\t\t\t\t\t\t\ttype=\"text\" formControlName=\"name\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Name here\" required /> <label for=\"name\">Username</label>\r\n\t\t\t\t\t\t\t\t\t</span> <span> <input class=\"clean-slide form-control\"\r\n\t\t\t\t\t\t\t\t\t\tid=\"password\" type=\"text\" formControlName=\"password\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"password here\" required /> <label for=\"password\">Password</label>\r\n\t\t\t\t\t\t\t\t\t</span> <span> <input class=\"clean-slide form-control\" id=\"cpassword\"\r\n\t\t\t\t\t\t\t\t\t\ttype=\"text\" formControlName=\"confirm_password\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"confirm password\" required /> <label\r\n\t\t\t\t\t\t\t\t\t\tfor=\"cpassword\">Confirm</label>\r\n\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t<span> <input class=\"clean-slide form-control\" id=\"email\"\r\n\t\t\t\t\t\t\t\t\t\ttype=\"text\" formControlName=\"confirm_password\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Email\" required /> <label\r\n\t\t\t\t\t\t\t\t\t\tfor=\"email\">Email</label>\r\n\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"row text-center\">\r\n\t\t\t\t\t\t\t\t<input type=\"submit\" class=\"btn fill-btn\"\r\n\t\t\t\t\t\t\t\t\tvalue=\"Register\" />\r\n\t\t\t\t\t\t\t\t<button routerLink=\"/login\" class=\"btn\">Login?</button>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/page/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterComponent = (function () {
    function RegisterComponent(http, fb, router) {
        this.http = http;
        this.fb = fb;
        this.router = router;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.userForm = this.fb.group({
            name: '',
            password: '',
            confirm_password: '',
            email: ''
        });
    };
    RegisterComponent.prototype.register = function (form) {
        var _this = this;
        console.log(form.value);
        this.http.post('user/register', form.value)
            .subscribe(function (res) {
            console.log(res);
            _this.router.navigateByUrl('/');
        });
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/page/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/page/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/service/login-status.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginStatusService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__ = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginStatusService = (function () {
    function LoginStatusService(http) {
        this.http = http;
        this.user = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["a" /* Subject */]();
    }
    LoginStatusService.prototype.post = function (name) {
        this.user.next(name);
    };
    LoginStatusService.prototype.subscribe = function () {
        return this.user.asObservable();
    };
    LoginStatusService.prototype.logout = function () {
        var _this = this;
        this.http.get('/user/logout').subscribe(function (res) {
            _this.post('guest');
        });
    };
    LoginStatusService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], LoginStatusService);
    return LoginStatusService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map